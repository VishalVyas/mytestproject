package com.mis.wamisjhmis.controllers;

import com.mis.wamisjhmis.healper.JwtUtil;
import com.mis.wamisjhmis.model.JwtRequest;
import com.mis.wamisjhmis.model.JwtResponse;
import com.mis.wamisjhmis.service.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JwtController {

    @Autowired
    private CustomUserDetailService customUserDetailServiceObj;
    @Autowired
    private JwtUtil jwtUtilObj;
    @Autowired
    private AuthenticationManager authenticationManagerObj;

    @PostMapping(value = "/token")
    public ResponseEntity<?> generateToken(@RequestBody JwtRequest jwtReqObj) throws Exception {
        System.out.println(jwtReqObj);

        try {
            this.authenticationManagerObj.authenticate(new UsernamePasswordAuthenticationToken(jwtReqObj.getUsername(), jwtReqObj.getPassword()));

        } catch (UsernameNotFoundException ex) {
            ex.printStackTrace();
            throw new Exception("User Name Not Found");
        }catch ( BadCredentialsException ex){
            ex.printStackTrace();
            throw new Exception("Bad Credintials");
        }
        UserDetails userDtlObj = this.customUserDetailServiceObj.loadUserByUsername(jwtReqObj.getUsername());
        String token = this.jwtUtilObj.generateToken(userDtlObj);
        System.out.println("JWT Token :- "+ token);

        return  ResponseEntity.ok( new JwtResponse(token) );
    }
}
