package com.mis.wamisjhmis.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {


    @GetMapping("/welcome")
    public String welcome(){
        String text="This is private page. ";
        text += "This page is not alllowed for un-authorized users.";
        return text;
    }



}
