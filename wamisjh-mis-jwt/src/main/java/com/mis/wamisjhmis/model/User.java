package com.mis.wamisjhmis.model;

import lombok.*;
import org.springframework.stereotype.Repository;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
@Repository
public class User {

    String userName;
    String email;
    String password;

}
