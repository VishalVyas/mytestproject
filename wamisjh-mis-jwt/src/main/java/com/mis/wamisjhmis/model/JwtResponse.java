package com.mis.wamisjhmis.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JwtResponse
{
    private String token;
}
