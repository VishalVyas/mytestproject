package com.mis.wamisjhmis.config;

import com.mis.wamisjhmis.healper.JwtUtil;
import com.mis.wamisjhmis.service.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter  extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtilObj;
    @Autowired
    private CustomUserDetailService customUserDetailServiceObj;

    /*
    * TODO:- Documentation of method
    *  doFilterInternal()
    *  The doFilterInternal() method is the main method of the JwtAuthenticationFilter class. This method is responsible for authenticating the user and setting the security context.
    *  Here is a step-by-step explanation of what happens in the doFilterInternal() method:
     * The method gets the JWT token from the Authorization header of the request.
     * If the token is valid, the method gets the username from the token.
     * The method loads the user details from the database using the CustomUserDetailService class.
     * The method creates a new UsernamePasswordAuthenticationToken object and sets the user details and authorities.
     * The method sets the authentication token in the security context.
     * If the token is invalid, the method prints a message to the console.
    * */

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
                //Steps :- ToDo in  doFiltwe method
                    // get jwt  Request Header
                    //  check starts with Bearer
                    // Validate the request

        String requestTokenheader = request.getHeader("Authorization");
        String username = null;
        String jwtToken = null;

        // we are checking for Header Formate and request is not null
        if(requestTokenheader.trim() != null && requestTokenheader.startsWith("Bearer ") ){
               jwtToken = requestTokenheader.substring(7);

               try {
                            username = jwtUtilObj.getUsernameFromToken(jwtToken);
               }catch (Exception ex){
                   ex.printStackTrace();
               }

            UserDetails userDetail = this.customUserDetailServiceObj.loadUserByUsername(username);

               // Start :- Most IMP Line of Code used for Authentication of a User Trying to loginto our applicaltion
                /*
                TODO:- Documentation OF IMP Method Snippet
                 * The if(username != null && SecurityContextHolder.getContext().getAuthentication() != null)
                 condition checks whether the username is not null and the authentication object in the security context is not null.
                 If both of these conditions are met, then a new UsernamePasswordAuthenticationToken object is created and set in the security context.
                 *
                 * The UsernamePasswordAuthenticationToken object is a concrete implementation of the Authentication interface.
                 It represents a user authentication request that is based on a username and password.
                 The constructor for the UsernamePasswordAuthenticationToken class takes three arguments:
                 *
                 * The principal, which is the user's identity.
                 * The credentials, which is the user's password.
                 * The authorities, which is a collection of authorities that the user has.
                 * In the example code, the principal is set to the userDetail object, which is a UserDetails object that contains the user's details.
                 The credentials are set to null, because the authentication has already been performed by the JwtAuthenticationFilter.
                 The authorities are set to the userDetail.getAuthorities() method, which returns a collection of authorities that the user has.
                 *
                 * The usernamePasswordAuthenticationTokenObj.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                 statement sets the details of the authentication token. The WebAuthenticationDetailsSource class provides a convenient way
                  to get the details of the authentication request. The buildDetails(request) method returns a WebAuthenticationDetails object,
                  which contains the following information:
                 *
                 * The remote address of the client.
                 * The session ID of the client.
                 * The request URL.
                 * The request headers.
                 * The SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationTokenObj); statement sets the
                  authentication token in the security context. The security context is a container that stores the security information for the
                  current request.
                 *
                 * Once the authentication token has been set in the security context, the user is considered to be authenticated.
                 This means that the user has access to the resources that are protected by Spring Security.
                 *
                 * Here is a more detailed explanation of each line of code in the if statement:
                 *
                 * ```Java
                 * if(username != null && SecurityContextHolder.getContext().getAuthentication() != null) {
                 *     UsernamePasswordAuthenticationToken usernamePasswordAuthenticationTokenObj = new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
                 *
                 *     usernamePasswordAuthenticationTokenObj.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                 *
                 *     SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationTokenObj);
                 * }
                 ```
                 * Use code with caution. Learn more
                 * username != null: This condition checks whether the username is not null. The username is obtained from the JWT token.
                 * SecurityContextHolder.getContext().getAuthentication() != null: This condition checks whether the authentication object
                  in the security context is not null. If the authentication object is null, then the user is not authenticated.
                 * new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities()): This creates a new
                 UsernamePasswordAuthenticationToken object. The principal is set to the userDetail object, which is a UserDetails object
                  that contains the user's details. The credentials are set to null, because the authentication has already been performed by the
                  JwtAuthenticationFilter. The authorities are set to the userDetail.getAuthorities() method, which returns a collection
                  of authorities that the user has.
                 * usernamePasswordAuthenticationTokenObj.setDetails(new WebAuthenticationDetailsSource().buildDetails(request)):
                 This sets the details of the authentication token. The WebAuthenticationDetailsSource class provides a convenient way to
                 get the details of the authentication request. The buildDetails(request) method returns a WebAuthenticationDetails object,
                 which contains the following information:
                 * The remote address of the client.
                 * The session ID of the client.
                 * The request URL.
                 * The request headers.
                 * SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationTokenObj): This sets the authentication
                 token in the security context. The security context is a container that stores the security information for the current request.
                 * Once the authentication token has been set in the security context, the user is considered to be authenticated.
                 This means that the user has access to the resources that are protected by Spring Security.
                * */
               if(username != null && SecurityContextHolder.getContext().getAuthentication() != null ){
                   UsernamePasswordAuthenticationToken usernamePasswordAuthenticationTokenObj= new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());

                   usernamePasswordAuthenticationTokenObj.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                   SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationTokenObj);
               }
               // End :- Most IMP Line of Code used for Authentication of a User Trying to loginto our applicaltion
               else{
                   System.out.println("Token is Invalid ...!!!!");
               }
        }
        filterChain.doFilter(request,response);

    }
}
