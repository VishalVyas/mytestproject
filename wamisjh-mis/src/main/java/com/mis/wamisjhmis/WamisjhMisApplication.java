package com.mis.wamisjhmis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WamisjhMisApplication {

	public static void main(String[] args) {
		SpringApplication.run(WamisjhMisApplication.class, args);
	}

}
