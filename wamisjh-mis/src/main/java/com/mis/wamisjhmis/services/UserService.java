package com.mis.wamisjhmis.services;


import com.mis.wamisjhmis.dao.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {


    List<User> list = new ArrayList<User>();

    public UserService() {
        list.add(new User("Vishal Vyas", "vishalv@cdac.in", "Wamis@123"));
        list.add(new User("Vrushabh Vaidya", "vrushabhv@cdac.in", "Wamis@789"));
    }

    // get list of user
    public List<User> getAllUsers() {
        return this.list;
    }

    //get single user
    public User getSingleUser(String name) {
        return this.list.stream().filter((user) -> user.getUserName().toLowerCase().contains(name.toLowerCase())).findAny().orElse(null);
    }

    // add new user to list
    public List<User> addUserToList(User u) {
        this.list.add(u);
        return this.list;
    }

}
