package com.mis.wamisjhmis.controllers;

import com.mis.wamisjhmis.dao.User;
import com.mis.wamisjhmis.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userServiceObj ;



    @GetMapping("/")
    public List<User> getuserList(){
        return userServiceObj.getAllUsers();
    }

    @GetMapping("/{name}")
    public User getUserByUserName( @PathVariable("name") String username ){
        return userServiceObj.getSingleUser(username);
    }

    @PostMapping("/")
    public List<User> addnewuserToList( @RequestBody User user){
        return userServiceObj.addUserToList(user);
    }
}
