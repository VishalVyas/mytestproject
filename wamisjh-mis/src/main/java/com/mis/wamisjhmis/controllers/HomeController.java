package com.mis.wamisjhmis.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class HomeController {


    @GetMapping("/")
    public String homepage(){
        return "Welcome  to  Home Page";
    }

    @GetMapping("/home")
    public String home(){
        return "Welcome  to  Home Page";
    }

    @GetMapping("/login")
    public String login(){
        return "Welcome  to  Login Page";
    }

    @GetMapping("/register")
    public String register(){
        return "Welcome  to  Register Page";
    }


}
