package com.mis.wamisjhmis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    @Override


    /*
    * This Java code configures HTTP Basic Authentication for a Spring Security application.
    * The @Configuration annotation tells Spring that this class contains configuration information.
    *  The @EnableWebSecurity annotation enables Spring Security in the application.
        The MySecurityConfig class extends WebSecurityConfigurerAdapter.
        This class provides a number of methods for configuring Spring Security. configure(HttpSecurity http)
        The configure(HttpSecurity http) method is used to configure the HTTP security for the application. In this case,
        *  the method is used to enable HTTP Basic Authentication and to require authentication for all requests
        *
        * BASIC AUthentication URL Base
        * // antMatcher ().permitAll() is used for providing access to selected URL's without authentication Open to all.
        *
        * Basically antMatcher() is used for performing operation on Spcific URI's
        *
        *
        * * ROLE BASED Authentication URL Base
        * antMatcher (). .hasRole("ADMIN")
    * */
    protected void configure(HttpSecurity http) throws Exception {

        http
//                .csrf().disable()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()) .and()// To every time send CSRF Token in cookie
                .authorizeRequests()
                .antMatchers("/home" , "/login" , "/register" , "/public/**").hasRole("NORMAL")
                .antMatchers("**/users/**").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
//                .httpBasic();           // FOR BASIC AUTHENTICATION
                .formLogin();            // FORM BASE AUTHENTICATION
    }

            /*
                The configure(AuthenticationManagerBuilder authObj) method is used to
                configure the authentication manager for the application. In this case,
                 the method is used to create two in-memory users: Vishal and Vrushabh.
                 The Vishal user has the ADMIN role, and the Vrushabh user has the NORMAL role.
             */
    @Override
    protected void configure(AuthenticationManagerBuilder authObj) throws Exception {
        authObj.inMemoryAuthentication().withUser("Vishal").password(this.passwordEncoder().encode("abc")).roles("ADMIN");
        authObj.inMemoryAuthentication().withUser("Vrushabh").password(this.passwordEncoder().encode("abc")).roles("NORMAL");
    }


            /*
                The passwordEncoder() method creates a new BCrypt password encoder.
                This encoder is used to encrypt the user passwords before they are stored in memory.\
            */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return  new BCryptPasswordEncoder(10);
    }
}
