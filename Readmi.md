<h3> Authentication Mechanisms </h3>

# Types of Authentication are :- 
    1. Username and Password - how to authenticate with a username/password
    2. OAuth 2.0 Login - OAuth 2.0 Log In with OpenID Connect and non-standard OAuth 2.0 Login (i.e. GitHub)
    3. SAML 2.0 Login - SAML 2.0 Log In
    4. Central Authentication Server (CAS) - Central Authentication Server (CAS) Support
    5. Remember Me - how to remember a user past session expiration
    6. JAAS Authentication - authenticate with JAAS
    7. Pre-Authentication Scenarios - authenticate with an external mechanism such as SiteMinder or Java EE security but still use Spring Security for
        authorization and protection against common exploits.
    8.  X509 Authentication - X509 Authentication


```
NOTE:- 
    # Role Base URL accessablity
        In Role Base Authentication we can restrict user to provide access or not to perticular Url in 

        WAY I :-  In our defined SecurityConfig Class
            ```java
            protected void configure(HttpSecurity http) throws Exception {

            http
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET,"/home" , "/login" , "/register" , "/public/**").hasRole("NORMAL")
                    .antMatchers(HttpMethod.GET,"/user/**").hasRole("ADMIN")
                    .anyRequest()
                    .authenticated()
                    .and()
                    .httpBasic();
        }
            ```
        WAY II:- using annotation @PreAuthorize("hasRole('ADMIN')") on methods to authorize to pirticular role 
        and 
        @EnableGlobalMethodSecurity in your configuration class 

```

# Form Based Authenication :- 

<img src="F:\Vishal\Development_Git\IntellijWorksplace\wamisjh-mis\src\main\images\FromBasedAuth.JPG">